console.log('Hello')

	// created an object using object literal

	let trainer =  {}
	console.log(trainer);

	// Initialized/ add the object properties and methods

	// Properties

	trainer.name = 'Ash Ketchum';
	trainer.age = 10;
	trainer.pokemon = ['Pikachu', 'Charizard', 'Squitrle', "Bulbasaur"];
	trainer.firends ={
		kanto: ['Brock', 'Misty'],
		hoenn: ['May', 'Max']
	} ;

	// methods
	trainer.talk = function() {
		console.log('Pikachu, I choose you!');
	};
	// Check all of the properties and methods were properly added
	console.log(trainer);

	// Access object properties and methods
	console.log('Result of dot notation');

	console.log(trainer.name);

	// Access object properties using square bracket notation

	console.log('Result of square bracket notation:');
	console.log(trainer['pokemon']);

	// Access the trainer 'talk' method
	console.log('Result of talk method')
	trainer.talk();

// reate aconstructor function for creating a pokemon.

function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	// Will accept an object as a target
	this.takle = function(target) {
		console.log(this.name + 'tackled' + target.name)
		target.health -= this.attack;
		console.log(target.name + "s' health is now reduced to " + target.health);

		if(target.health <= 0){
			target.faint()
		}
	};

	this.faint =function(){
		console.log(this.name + 'fainted.')
	}
}

//  Create/instantiate a new pokemon

let pikachu = new Pokemon ('Pikachu', 12);
console.log(pikachu);

// Create/instantiate a new pokemon 
let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

// Create/instantiate a new pokemon 
let mewto = new Pokemon('Mewto', 100);
console.log(mewto);

// Invoke the takle method and target a diff object

geodude.takle(pikachu);
console.log(pikachu);

// Invoke the takle method and target diff. object

mewto.takle(geodude);
console.log(geodude);






















